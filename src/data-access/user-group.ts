import { Model, UUID } from 'sequelize';
import { DataBaseService } from '../services/data-base.service.';
import { User } from './user';
import { Group } from './group';

const db = DataBaseService.getInstance().db;

export class UserGroup extends Model {}
UserGroup.init({
  user_id: UUID,
  group_id: UUID,
}, { sequelize: db, modelName: 'usergroups'});
User.belongsToMany(Group, { through: 'usergroups', foreignKey: 'user_id' });
Group.belongsToMany(User, { through: 'usergroups', foreignKey: 'group_id'});
