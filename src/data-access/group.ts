import { ARRAY, Model, STRING, UUIDV4 } from 'sequelize';
import { ErrorCodes, permissions } from '../constants';
import { HttpException } from '../middlewares/error.middleware';
import { DataBaseService } from '../services/data-base.service.';

const db = DataBaseService.getInstance().db;

export class Group extends Model {}
Group.init({
  name: {
    allowNull: false,
    type: STRING,
  },
  permissions: {
    allowNull: false,
    type: ARRAY(STRING),
    validate: {
      isElementsValid: (value) => {
        if (!value) {
          return;
        }

        const values = Array.isArray(value) ? value : [value];

        values.forEach((item) => {
          if (!permissions.includes(item)) {
            throw new HttpException(ErrorCodes.BadRequest, 'permission array could contain only READ, WRITE, DELETE, SHARE, UPDATE_FILES strings');
          }
        });

        return value;
      },
    },
  },
}, { sequelize: db, modelName: 'groups'});
