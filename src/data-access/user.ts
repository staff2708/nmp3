import { BOOLEAN, INTEGER, Model, STRING } from 'sequelize';
import { maxAge, minAge, passwordRegExp } from '../constants';
import { DataBaseService } from '../services/data-base.service.';

const db = DataBaseService.getInstance().db;

export class User extends Model {}
User.init({
  age: {
    allowNull: false,
    type: INTEGER,
    validate: {
      max: maxAge,
      min: minAge,
    },
  },
  deleted: {
    allowNull: false,
    type: BOOLEAN,
  },
  login: {
    type: STRING,
    unique: true,
  },
  password: {
    type: STRING,
    validate: {
      is: passwordRegExp,
    },
  },
}, { sequelize: db, modelName: 'users' });
