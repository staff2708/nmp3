export const passwordRegExp = /^[a-zA-Z0-9]+$/;
export const minAge = 4;
export const maxAge = 130;
export const permissions = ['READ', 'WRITE', 'DELETE', 'SHARE', 'UPLOAD_FILES'];

export enum ErrorCodes {
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  InternalServerError = 500,
}
export const passwordRegexpPattern = new RegExp('.', 'g');
export const SECRET = 'testPrivateKey';
