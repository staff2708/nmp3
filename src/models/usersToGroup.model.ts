import { array, object, string } from '@hapi/joi';

interface IUsersToGroupSchema {
  usersIds: string[];
  groupId: string;
}

export const usersToGroupSchema = object<IUsersToGroupSchema>({
  usersIds: array().required().items(string().uuid()),
  groupId: string().required().uuid(),
});
