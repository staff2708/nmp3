import * as joi from '@hapi/joi';
import { IGroup } from '../interfaces/group.interface';

export const groupSchema = joi.object<IGroup>({
  id: joi.string().uuid(),
  name: joi.string().required(),
  permissions: joi.array().required().items(joi.string().valid('READ', 'WRITE', 'DELETE', 'SHARE', 'UPLOAD_FILES')),
});

export const groupUpdateSchema = joi.object<IGroup>({
  name: joi.string(),
  permissions: joi.array().items(joi.string().valid('READ', 'WRITE', 'DELETE', 'SHARE', 'UPLOAD_FILES')),
});
