import * as joi from '@hapi/joi';
import { IWithId } from '../interfaces/withId.interface';

export const withIdSchema = joi.object<IWithId>({
  id: joi.string().required().uuid(),
});
