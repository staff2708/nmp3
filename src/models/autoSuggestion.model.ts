import * as joi from '@hapi/joi';
import { IAutoSuggestion } from '../interfaces/autoSuggestion.interface';

export const autoSuggestionSchema = joi.object<IAutoSuggestion>({
  limit: joi.number().required(),
  substring: joi.string().required(),
});
