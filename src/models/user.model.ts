import * as joi from '@hapi/joi';
import { maxAge, minAge, passwordRegExp } from '../constants';
import { IUser } from '../interfaces/user.interface';

export const userSchema = joi.object<Partial<IUser>>({
  age: joi.number().required().min(minAge).max(maxAge),
  login: joi.string().required(),
  password: joi.string().required().regex(passwordRegExp),
});

export const loginUserSchema = joi.object<Partial<IUser>>({
  login: joi.string().required(),
  password: joi.string().required(),
});

export const userUpdateSchema = joi.object<Partial<IUser>>({
  age: joi.number().min(minAge).max(maxAge),
  deleted: joi.boolean(),
  login: joi.string(),
  password: joi.string().regex(passwordRegExp),
});
