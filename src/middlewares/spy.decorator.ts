import { passwordRegexpPattern } from '../constants';
import {logger} from '../utils/logger';
export function spy() {
  return function(target, propertyName, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = async function(...args) {
      const logArgs = args.map((item) => {
        let pass;
        if (item.password) {
          pass = item.password.replace(passwordRegexpPattern, '*');
        }
        return item.password ? {...item, password: pass} : item;
      });
      logger.info(`Function ${propertyName} was called with this args: ${JSON.stringify(logArgs)}`);
      try {
        return await originalMethod.apply(this, args);
      } catch (e) {
        const logArgsCatch = args.map((item) => {
          let pass;
          if (item.password) {
            pass = item.password.replace(passwordRegexpPattern, '*');
          }
          return item.password ? {...item, password: pass} : item;
        });
        logger.error(`Error occure in function ${propertyName}, when it was called with args: ${JSON.stringify(logArgsCatch)}, err message: ${e.message}`);
        throw e;
      }
    };
  };
}
