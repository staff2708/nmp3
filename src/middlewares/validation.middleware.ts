import { ObjectSchema, ValidationErrorItem } from '@hapi/joi';
import { ErrorCodes } from '../constants';

// mapper
function errorResponse(schemaErrors: ValidationErrorItem[]) {
  const errors = schemaErrors.map((error) => {
    const { path, message } = error;
    return { path, message };
  });

  return {
    errors,
    status: 'failed',
  };
}

export function validateSchema(schema: ObjectSchema, place: string = 'body') {
  return (req, res, next) => {
    const { error } = schema.validate(req[place], {
      allowUnknown: false,
    });

    if (error && error.isJoi) {
      res.status(ErrorCodes.BadRequest).json(errorResponse(error.details));
    } else {
      next();
    }
  };
}
