import { NextFunction, Request, Response } from 'express';
import { Logger } from 'winston';

export class HttpException extends Error {
  constructor(public status: number, public message: string) {
    super(message);
  }
}

export function errorLoggerMiddleware(
  logger: Logger,
) {
  return (
    error: HttpException,
    request: Request,
    response: Response,
    next: NextFunction,
  ) => {
    logger.log({
      level: 'error',
      message: `Code: ${error.status || 500}, Message: ${error.message || 'Something went wrong'}`,
    });
    next(error);
  };
}

function errorMiddleware(
  error: HttpException,
  request: Request,
  response: Response,
  next: NextFunction,
) {
  const status = error.status || 500;
  const message = error.message || 'Something went wrong';
  response
    .status(status)
    .send({
      message,
      status,
    });
}

export default errorMiddleware;
