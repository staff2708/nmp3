import {logger} from "../utils/logger";

export function timeTracker() {
  return function(target, propertyName, descriptor) {
    const originalMethod = descriptor.value;
    const startTime = process.hrtime();
    descriptor.value = function(...args) {
      originalMethod.apply(this, args);
      const endTime = process.hrtime(startTime)[1];
      logger.log('info', `Function ${propertyName} execution time ${endTime / 1000000000} second`);
    };
  };
}
