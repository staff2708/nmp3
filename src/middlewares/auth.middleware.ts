import * as express from 'express';
import { ErrorCodes } from '../constants';
import { AuthorizationService } from '../services/authorization.service';
import { HttpException } from './error.middleware';

const authService = AuthorizationService.getInstance();

export async function authMiddleware(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) {
  try {
    if (req.originalUrl !== '/api/user/login') {
      await authService.validateToken(req.headers.authorization);
    }
    next();
  } catch (e) {
    next(new HttpException(ErrorCodes.Unauthorized, e.message));
  }
}
