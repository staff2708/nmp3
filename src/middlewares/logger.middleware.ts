import { NextFunction, Request, Response } from 'express';
import { Logger } from 'winston';
import { passwordRegexpPattern } from '../constants';

export function loggerMiddleware(
  logger: Logger,
) {
  return (
    request: Request,
    response: Response,
    next: NextFunction,
  ) => {
    const { method, path, body} = request;
    const logItem = {...body};
    if (logItem.password) {
      logItem.password = body.password.replace(passwordRegexpPattern, '*');
    }
    const message = `${method} ${path} ${body ? 'body:' + JSON.stringify(logItem) : ''}`;
    logger.log({
      level: 'info',
      message,
    });
    next();
  };
}
