export const defaultState = [
  {
    age: 19,
    login: 'JHON',
    password: '12345678',
  },
  {
    age: 22,
    login: 'Alex',
    password: '87654321',
  },
  {
    age: 42,
    login: 'Admin',
    password: 'VerySecretPassword',
  },
  {
    age: 27,
    login: 'Vlad',
    password: 'password123',
  },
];
