export interface IUser {
  id?: string;
  age: number;
  login: string;
  password: string;
  deleted: boolean;
}
