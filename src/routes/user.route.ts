import * as express from 'express';
import { IRouter, NextFunction } from 'express';
import { ErrorCodes } from '../constants';
import { IUser } from '../interfaces/user.interface';
import {HttpException} from '../middlewares/error.middleware';
import { validateSchema } from '../middlewares/validation.middleware';
import { autoSuggestionSchema } from '../models/autoSuggestion.model';
import {loginUserSchema, userSchema, userUpdateSchema} from '../models/user.model';
import { withIdSchema } from '../models/withId.model';
import {CryptographyService} from '../services/cryptography.service';
import { UserService } from '../services/user.service';

export class UserRouter {

  private path = '/user';
  private router: IRouter = express.Router();

  constructor(
    private userService: UserService,
    private cryptographyService: CryptographyService,
  ) {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, this.getAllUsers);
    this.router.get(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.getUserById);
    this.router.post(`${this.path}/`, validateSchema(userSchema), this.addUser);
    this.router.post(`${this.path}/login`, validateSchema(loginUserSchema), this.login);
    this.router.post(`${this.path}/as`, validateSchema(autoSuggestionSchema), this.getAutoSuggest);
    this.router.delete(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.removeUser);
    this.router.put(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), validateSchema(userUpdateSchema), this.updateUser);
  }

  private getAllUsers = async (request: express.Request, response: express.Response, next: NextFunction) => {
    try {
      const users: IUser[] = await this.userService.getAllUsers();
      response.status(200).json(users);
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private getUserById = async (request: express.Request, response: express.Response, next: NextFunction) => {
    try {
      const user: IUser = await this.userService.getUserById(request.params.id);
      if (user && !user.deleted) {
        delete user.password;
        response.status(200).json(user);
      } else {
        next(new HttpException(ErrorCodes.NotFound, 'User not found'));
      }
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private addUser = async (request: express.Request, response: express.Response, next: NextFunction) => {
    const { login, age } = request.body;
    const password = await this.cryptographyService.encrypt(request.body.password);
    try {
      const [user, isUserCreated] = await this.userService.addUser({ login, password, age: +age, deleted: false });
      if (isUserCreated) {
        response.status(200).send({ id: user.id, login, age: +age });
      } else {
        throw new HttpException(ErrorCodes.BadRequest, 'User already exist');
      }
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private updateUser = async (request: express.Request, response: express.Response, next: NextFunction) => {
    const { id } = request.params;
    const updatedUserData = { ...request.body };
    try {
      if (typeof updatedUserData.age === 'string') {
        updatedUserData.age = +updatedUserData.age;
      }
      if (updatedUserData.password) {
        updatedUserData.password = await this.cryptographyService.encrypt(updatedUserData.password);
      }
      if (updatedUserData.id) {
        delete updatedUserData.id;
      }
      const res = await this.userService.updateUser(id, { ...updatedUserData });
      if (res[0]) {
        response.status(200).send();
      } else {
        next(new HttpException(ErrorCodes.BadRequest, 'Error, User not exist or fields was not updated'));
      }
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private removeUser = async (request: express.Request, response: express.Response, next: NextFunction) => {
    const { id } = request.params;
    try {
      const isDeleted = await this.userService.deleteUser(id);
      if (!isDeleted[0]) {
        throw new HttpException(ErrorCodes.BadRequest, 'Invalid id');
      }
      response.status(200).send();
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private getAutoSuggest = async (request: express.Request, response: express.Response, next: NextFunction) => {
    const { substring, limit } = request.body;
    try {
      const users = await this.userService.getAutoSuggestUsers(substring, limit);
      response.status(200).json(users);
    } catch (e) {
      next(new HttpException(e.status, e.message));
    }
  }

  private login = async (req: express.Request, res: express.Response, next: NextFunction) => {
    const { login, password } = req.body;
    const hashPassword =  await this.cryptographyService.encrypt(password);
    try {
      const token = await this.userService.authenticate({ login, password: hashPassword });
      if (token) {
        res.status(200).json({ token });
      } else {
        throw new HttpException(ErrorCodes.NotFound, 'There is no user with such login and password');
      }
    } catch (e) {
      next(new HttpException( e.status || ErrorCodes.Unauthorized, e.message));
    }
  }

}
