import { IRouter, NextFunction, Request, Response, Router } from 'express';
import {HttpException} from '../middlewares/error.middleware';
import { validateSchema } from '../middlewares/validation.middleware';
import { groupSchema, groupUpdateSchema } from '../models/group.model';
import { usersToGroupSchema } from '../models/usersToGroup.model';
import { withIdSchema } from '../models/withId.model';
import { GroupService } from '../services/group.service';
import { ErrorCodes } from '../constants';

export class GroupRoute {
  private path = '/group';
  private router: IRouter = Router();

  constructor(
    private groupService: GroupService,
  ) {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/`, this.getGroups);
    this.router.get(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.getGroupById);
    this.router.post(`${this.path}/`, validateSchema(groupSchema), this.createGroup);
    this.router.put(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), validateSchema(groupUpdateSchema), this.updateGroup);
    this.router.delete(`${this.path}/:id`, validateSchema(withIdSchema, 'params'), this.deleteGroup);
    this.router.post(`${this.path}/addUsersToGroup`, validateSchema(usersToGroupSchema), this.addToGroup);
  }

  private getGroups = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    try {
      const groups = await this.groupService.getGroups();
      response.status(200).json(groups);
    } catch (e) {
      next(new HttpException(ErrorCodes.InternalServerError, e.message));
    }
  }

  private getGroupById = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    try {
      const id = request.params.id;
      const group = await this.groupService.getGroupById(id);
      if (group) {
        response.status(200).json(group);
      } else {
        next(new HttpException(ErrorCodes.NotFound, 'Group not found'));
      }
    } catch (e) {
      next(new HttpException(ErrorCodes.InternalServerError, e.message));
    }
  }

  private createGroup = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    try {
      const [group, isGroupCreated] = await this.groupService.createGroup( { ...request.body });
      if (isGroupCreated) {
         response.status(200).json({ id: group.id });
      } else {
        throw new HttpException(ErrorCodes.BadRequest, 'Group already exist');
      }
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private updateGroup = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    const id = request.params.id;
    const updatedGroupData = { ...request.body };

    try {
      if (updatedGroupData.id) {
        delete updatedGroupData.id;
      }

      const res = await this.groupService.updateGroup(id, updatedGroupData);
      if (res[0]) {
        response.status(200).send();
      } else {
        next(new HttpException(ErrorCodes.BadRequest, 'Error, Group not exist or fields was not updated'));
      }
    } catch (e) {
      next(e);
    }
  }

  private deleteGroup = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    const id = request.params.id;

    try {
      const res = await this.groupService.deleteGroup(id);
      if (res && res.group === null) {
        throw new HttpException(ErrorCodes.NotFound, 'Group not found');
      }
      response.status(200).send();
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }

  private addToGroup = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
    const { usersIds, groupId } = request.body;
    try {
      const res = await this.groupService.addToGroup(groupId, usersIds);
      if (res && ( res.group === null || res.user === null)) {
        throw new HttpException(ErrorCodes.NotFound, res && res.group === null ? 'Group not found' : 'User nor found');
      } else {
        response.status(200).send();
      }
    } catch (e) {
      next(new HttpException(e.status || ErrorCodes.InternalServerError, e.message));
    }
  }
}
