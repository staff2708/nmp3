import {createLogger, format, transports} from 'winston';

export const logger = createLogger({
    transports: [
        new transports.Console(
            {
                format: format.combine(
                    format.colorize(
                        {
                            all: true,
                            colors: {
                                info: 'blue',
                                warn: 'yellow',
                                error: 'red',
                            },
                        }),
                    format.timestamp(),
                    format.printf((info) => {
                        const {
                            timestamp, level, message,
                        } = info;

                        return `${timestamp} ${level}: ${message}`;
                    }),
                ),
            },
        ),
    ],
});
