import * as cors from 'cors';
import * as express from 'express';
import { Sequelize } from 'sequelize';
import errorMiddleware, { errorLoggerMiddleware } from './middlewares/error.middleware';
import { loggerMiddleware } from './middlewares/logger.middleware';
import { timeTracker } from './middlewares/timetracker';
import { logger } from './utils/logger';
import { authMiddleware } from './middlewares/auth.middleware';

class App {
  public app: express.Application;
  public port: number;

  constructor(routes: any[], port: number, db: Sequelize) {
    this.initLogger();
    this.app = express();
    this.port = port;
    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    this.initErrorMiddleware();
  }

  @timeTracker()
  public listen(): void {
    this.app.listen(this.port, () => {
      logger.log('info', `App listening on the port ${this.port}`);
    });
  }

  private initializeMiddlewares(): void {
    this.app.use(express.json());
    this.app.use(cors());
    this.app.use(loggerMiddleware(logger));
    this.app.use(authMiddleware);
  }

  @timeTracker()
  private initializeRoutes(routes): void {
    routes.forEach((route) => {
      this.app.use('/api', route.router);
    });
  }

  @timeTracker()
  private initErrorMiddleware(): void {
    this.app.use(errorLoggerMiddleware(logger));
    this.app.use(errorMiddleware);
  }

  @timeTracker()
  private initLogger(): void {
    process.on('uncaughtException', (err) => {
      logger.log({
        message: err.message,
        level: 'error',
      });
    });
    process.on('unhandledRejection', (err: Error | undefined) => {
      logger.log({
        message: err && err.message || 'unhandled promise rejection',
        level: 'warn',
      });
    });
  }
}

export default App;
