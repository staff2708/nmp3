import { Sequelize } from 'sequelize';
import { CryptographyService } from './cryptography.service';

export class DataBaseService {

  public static getInstance() {
    if (!this.database) {
      this.database = new DataBaseService();
    }
    return this.database;
  }

  private static database: DataBaseService;
  public db: Sequelize;

  private constructor() {
    this.db = new Sequelize('NMPTask', 'postgres', 'staff2708', {
      define: {
        createdAt: false,
        updatedAt: false,
      },
      dialect: 'postgres',
      host: 'localhost',
      port: 5432,
      logging: false,
    });
    this.initDB();
  }

  private async initDB() {
    await this.db.authenticate();
    await this.db.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
    await this.db.query('DROP TABLE IF EXISTS usergroups');
    await this.db.query('DROP TABLE IF EXISTS users');
    await this.db.query('DROP TABLE IF EXISTS groups');
    await this.db.query(
      `CREATE TABLE IF NOT EXISTS users (id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (), age int, login varchar(255), password varchar(255), deleted boolean);`,
    );
    await this.db.query(
      `CREATE TABLE IF NOT EXISTS groups (id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (), name text, permissions text[]);`,
    );
    await this.db.query(
      `CREATE TABLE IF NOT EXISTS usergroups (id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (),user_id UUID REFERENCES users (id) ON DELETE CASCADE, group_id UUID REFERENCES groups (id) ON DELETE CASCADE);`,
    );

    const crypto = new CryptographyService();
    const pass = await crypto.encrypt('1234');
    await this.db.query(
      `INSERT INTO users (login, age, password, deleted) values ('admin', 22, '${pass}', false);`,
    );

  }
}
