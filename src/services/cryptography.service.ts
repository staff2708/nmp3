import * as crypto from 'crypto';

export class CryptographyService {
    private algorithm = 'aes-256-cbc';
    private iv: Buffer = Buffer.from('SomeSecretString');
    private key: Buffer = Buffer.from('SomeSecretStringSomeSecretString');

    public async encrypt(text: string): Promise<string> {
        const cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return encrypted.toString('hex');
    }

    public async decrypt(text: string): Promise<string> {
        const encryptedText = Buffer.from(text, 'hex');
        const decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }
}
