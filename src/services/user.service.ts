import { Op } from 'sequelize';
import { Group } from '../data-access/group';
import { User } from '../data-access/user';
import { IUser } from '../interfaces/user.interface';
import { spy } from '../middlewares/spy.decorator';
import { AuthorizationService } from './authorization.service';

export class UserService {
    constructor(private auth: AuthorizationService) {
    }

    @spy()
    public async getUserById(id: string): Promise<IUser> {
        return await User.findByPk(id, {
            attributes: ['id', 'age', 'login', 'deleted'],
            include: [{
                model: Group,
            }],
        });
    }

    @spy()
    public async getAllUsers(): Promise<IUser[]> {
        return await User.findAll({
            attributes: ['id', 'age', 'login', 'deleted'],
            order: [
                ['login', 'ASC'],
            ],
            where: {
                [Op.not]: {deleted: true},
            },
            include: [{
                model: Group,
                through: {
                    attributes: [],
                },
            }],
        });
    }

    @spy()
    public async addUser(newUser: IUser): Promise<[IUser, boolean]> {
        return await User.findOrCreate({
            defaults: {login: newUser.login},
            where: {...newUser},
        });
    }

    @spy()
    public async deleteUser(id: string): Promise<any[]> {
        return await User.update(
            {
                deleted: true,
            },
            {
                where: {id},
            });
    }

    @spy()
    public async getAutoSuggestUsers(loginSubstring: string, limit: number): Promise<IUser[]> {
        return await User.findAll({
            attributes: ['id', 'login', 'age', 'deleted'],
            limit,
            where: {
                login: {
                    [Op.like]: `%${loginSubstring}%`,
                },
                [Op.not]: { deleted: true },
            },
        });
    }

    @spy()
    public async updateUser(id: string, data: IUser): Promise<void> {
        return await User.update(
            {...data},
            {where: {id}},
        );
    }

    @spy()
    public async authenticate({ login, password: hashedPassword }): Promise<string | null> {
        return this.auth.signUser({ login, password: hashedPassword });
    }
}
