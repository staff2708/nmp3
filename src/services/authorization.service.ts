import { sign, verify } from 'jsonwebtoken';
import { Op } from 'sequelize';
import { SECRET } from '../constants';
import { User } from '../data-access/user';
import { IUser } from '../interfaces/user.interface';

export class AuthorizationService {

    public static getInstance(): AuthorizationService {
        if (this.instance) {
            return this.instance;
        }
        return (this.instance = new AuthorizationService());
    }

    private static instance: AuthorizationService;

    private constructor() {}

    public async signUser({ password: hashedPassword, login }): Promise<string> {
        const foundUser = await User.findOne({
            where: {
                login,
                [Op.not]: { deleted: true },
            },
            attributes: ['login', 'password'],
        });
        if (!foundUser) {
            throw new Error(
              `No such user with login '${login}' and provided password.`,
            );
        }
        if (hashedPassword !== foundUser.password) {
            throw new Error('Password is invalid');
        }
        return sign(foundUser.dataValues, SECRET);
    }
    public async validateToken(token: string): Promise<void> {
        if (!token) {
            throw new Error('Auth token is missing.');
        }
        const { password, login } = this.getDataFromToken(token);

        const user = await User.findOne({
            where: {
                login,
                password,
            },
        });

        if (user && user.password !== password) {
            throw new Error('Invalid password.');
        }

        if (!(password && login)) {
            throw new Error('Invalid access token.');
        }

        return;
    }

    private getDataFromToken(token: string): Pick<IUser, 'password' | 'login'> {
        return verify(token, SECRET) as Pick<IUser, 'password' | 'login'>;
    }
}
