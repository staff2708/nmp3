import { Group } from '../data-access/group';
import { User } from '../data-access/user';
import { UserGroup } from '../data-access/user-group';
import { IGroup } from '../interfaces/group.interface';
import { spy } from '../middlewares/spy.decorator';
import { DataBaseService } from './data-base.service.';

export class GroupService {

  private db = DataBaseService.getInstance().db;

  @spy()
  public async getGroups(): Promise<IGroup[]> {
      return await Group.findAll({
        attributes: ['id', 'name', 'permissions'],
        include: [{
          model: User,
          as: 'users',
          where: { deleted: false },
          attributes: ['id', 'age', 'login'],
          required: false,
          through: {
            attributes: [],
          },
        }],
      });
  }

  @spy()
  public async getGroupById(id: string): Promise<IGroup> {
      return await Group.findByPk(id, {
        attributes: ['id', 'name', 'permissions'],
        include: [{
          model: User,
          where: { deleted: false },
          attributes: ['id', 'age', 'login'],
          required: false,
          through: {
            attributes: [],
          },
        }],
      });
  }

  @spy()
  public async createGroup(newGroup: IGroup): Promise<[IGroup, boolean]> {
    return await Group.findOrCreate({
      defaults: { ...newGroup },
      where: { name: newGroup.name },
    });
  }

  @spy()
  public async updateGroup(id: string, data: IGroup): Promise<void> {
    return await Group.update(
      { ...data },
      { where: { id } },
    );
  }

  @spy()
  public async deleteGroup(id: string): Promise<any> {
    const transaction = await this.db.transaction();

    const group = await Group.findByPk(id, {
        attributes: ['id', 'name', 'permissions'],
        transaction,
      });

    if (group) {
        await Group.destroy({ where: {id}, transaction });
      } else {
        if (transaction) {
          await transaction.rollback();
        }
        return { group: null };
      }

    await transaction.commit();
    return group;
  }

  @spy()
  public async addToGroup(groupId: string, usersIds: string[]): Promise<any> {
    let transaction;
    try {
      transaction = await this.db.transaction();

      const group = await Group.findByPk(groupId, {
        attributes: ['id', 'name', 'permissions'],
        transaction,
      });

      if (group) {
        for (const userId of usersIds) {
          const isUserWithId = await User.findByPk(userId, {
            attributes: ['id'],
            transaction,
          });

          if (isUserWithId) {
            await UserGroup.findOrCreate({
              defaults: { user_id: userId, group_id: groupId },
              where: { user_id: userId, group_id: groupId },
              transaction,
            });
          } else {
            if (transaction) {
              await transaction.rollback();
            }
            return { user: null };
          }
        }
        await transaction.commit();
      } else {
        if (transaction) {
          await transaction.rollback();
        }
        return { group: null };
      }
    } catch (e) {
      if (transaction) {
        await transaction.rollback();
      }
      throw e;
    }
  }
}
