import App from './src/app';
import { GroupRoute } from './src/routes/group.route';
import { UserRouter } from './src/routes/user.route';
import {AuthorizationService} from './src/services/authorization.service';
import {CryptographyService} from './src/services/cryptography.service';
import { DataBaseService } from './src/services/data-base.service.';
import { GroupService } from './src/services/group.service';
import { UserService } from './src/services/user.service';

const authService = AuthorizationService.getInstance();
const userService = new UserService(authService);
const cryptoService = new CryptographyService();
const groupService = new GroupService();
const db = DataBaseService.getInstance().db;

const app = new App(
  [
    new UserRouter(userService, cryptoService),
    new GroupRoute(groupService),
  ],
  5000,
  db,
);

app.listen();
